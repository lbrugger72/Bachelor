import math
from math import sqrt, cos, asin, pi
from typing import Tuple

import simpy
import csv

from infrastructure import DataCenter, Link, Source
from leaf.application import Task
from settings import *
from leaf.infrastructure import Infrastructure
from orchestratorRR import orchestratorRR
from orchestratorWSNB import orchestratorWSNB
from orchestratorMinBrown import orchestratorMinBrown
from orchestratorCRADP import orchestratorCRADP

class Network:      # contains the environment and chosen orchestrator (strategy), and sets up the network components defined in 'infrastructure'
    def __init__(self, env: simpy.Environment, orchestrator: int):  # 'orchestrator' determines which selection strategy will be implemented
        self.env = env
        self.infrastructure = Infrastructure()
        if orchestrator == 1:
            self.orchestrator = orchestratorWSNB(self.infrastructure)
            print(f"\nSELECTION STRATEGY IS 'WSNB'")
        elif orchestrator == 2:
            self.orchestrator = orchestratorMinBrown(self.infrastructure)
            print(f"\nSELECTION STRATEGY IS 'MinBrown'")
        elif orchestrator == 3:
            self.orchestrator = orchestratorCRADP(self.infrastructure)
            print(f"\nSELECTION STRATEGY IS 'CRA-DP'")
        else:
            self.orchestrator = orchestratorRR(self.infrastructure)
            print(f"\nSELECTION STRATEGY IS 'Round Robin'")
        self.fallBack = orchestratorRR(self.infrastructure)
        self.orchestratorID = orchestrator
        self.dcLocations = DC_LOCATIONS
        self.srcLocations = SRC_LOCATIONS
        self.queuedTasks: list[Tuple[Task, int, int]] = []

        self.sourceCount = [0,0,0,0,0,0,0]
        self.distributionCount = [0,0,0,0,0]
        self.fallBackCount = 0
        self.dropppedRequests = []

        # Create cloud network infrastructure
        for i, location in enumerate(self.dcLocations):
            offset = (1440 + round(location[0]*4))%1440
            self.newDCNode(location, i, offset)
        for i, location in enumerate(self.srcLocations):
            self.newSRCNode(location, i)
        self.env.process(self.addRequestProcesses())

    def newDCNode(self, location: Tuple[int, int], id: int, offset: int):       # sets up a new data center node
        dataCenter = DataCenter(location, id, self.env, offset)
        self.infrastructure.add_node(dataCenter)


    def newSRCNode(self, location: Tuple[int, int], id: int):           # sets up a new source node and its links to all data center nodes
        source = Source(location, id)
        self.infrastructure.add_node(source)
        for center in self.infrastructure.nodes(type_filter=DataCenter): # add links to all data centers
            self.infrastructure.add_link(Link(source, center, SRC_LATENCY_TABLE[source.id][center.id]))   # current formula is placeholder for latency (calculated by way of linear regression from data about distances, latencies from Frankfurt to other cities)

    def addRequestProcesses(self):    # reads the tasks out of request file and places them somewhere on the cloud infrastructure
        with open(TASK_DATA_FILE, encoding='utf-8-sig') as f:
            readCSV = csv.reader(f, delimiter=',')
            sortedRequests = sorted(readCSV, key=lambda row: int(row[0]), reverse=False)
        for i in range(len(sortedRequests)):
            task = self.generateRequest(sortedRequests[i])  # generate a task from the line representing the request in the csv
            sourceId = int(sortedRequests[i][3]) % len(SRC_LOCATIONS)
            lifetime = math.ceil((int(sortedRequests[i][1])/1000000 - int(sortedRequests[i][0])/1000000)/60)

            self.sourceCount[sourceId] += 1

            successfulPlacement, latency, avgResponseTime = self.orchestrator.place(task,sourceId)  # pass task and source id to orchestrator for placement. NOTE: latency and avgResponseTime are not actual values unless passed by the fallback strategy
            if successfulPlacement:
                self.env.process(self.removeRequest(task, lifetime))  # lifetime of the request = end timestamp - start timestamp
            else:
                self.fallBackCount += 1
                successfulPlacement, latency, avgResponseTime = self.fallBack.place(task, sourceId)
                if successfulPlacement:
                    self.orchestrator.responseTimes.append(latency)
                    self.orchestrator.avgResponseTime = avgResponseTime
                    self.env.process(self.removeRequest(task, lifetime))
                else:
                    self.droppedRequests.append(task)
            if i < len(sortedRequests) - 1:
                yield self.env.timeout(math.ceil((int(sortedRequests[i + 1][0])/1000000/60) - math.ceil(int(sortedRequests[i][0])/1000000/60)))  # timeout until the starting timestamp of the next request

    def generateRequest(self, request: list[str]):      # creates the actual task based on scaling the given memory usage value to MIPS
        newTask = Task(mips=int(float(request[5])*UTILIZATION_SCALING))  # scales the given average memory usage of the request with the scaling variable determined by the maximum mips usage of a task (rounded to the nearest int)
        return newTask

    def removeRequest(self, task: Task, lifeTime: int):     # removes a task from its allocated node when its given lifetime/runtime expires
        yield self.env.timeout(lifeTime)

        self.distributionCount[task.node.dataC.id] += 1

        task.deallocate()

    def calculateDistance(self, location1: Tuple[int, int], location2: Tuple[int, int]):    # returns the distance between 2 longitude/latitude tuples in terms of kilometers using Haversine formula
        p = pi / 180
        a = 0.5 - cos((location2[1] - location1[1]) * p) / 2 + cos(location1[1] * p) * cos(location2[1] * p) * (1 - cos((location2[0] - location1[0]) * p)) / 2
        return 12742 * asin(sqrt(a))