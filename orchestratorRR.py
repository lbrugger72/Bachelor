import math

import logging

from infrastructure import DataCenter, Link, Source, Server
from leaf.application import Task
from leaf.infrastructure import Infrastructure, Node
from leaf.orchestrator import Orchestrator
from settings import DC_LOCATIONS

logger = logging.getLogger(__name__)

class orchestratorRR(Orchestrator):

    def __init__(self, infrastructure: Infrastructure):
        super().__init__(infrastructure)
        self.responseTimes = []
        self.avgResponseTime = 0
        self.placementCount = 0     # keeps track of how many placements have been done to ensure that tasks are evenly distributed between each data center

    def place(self, task: Task, source: int, threshold=100):    # allocates the given task to the node found by _processing_task_placement(), then returns whether placement was successful or not and response time values
        node, latency, avgResponseTime = self._processing_task_placement(task, source)
        if node is not None:
            task.id = 1  # task id is set to 1 only at full placement (to set apart tasks that are only being provisionally allocated to calculate power usage during placement)
            task.allocate(node)
            self.placementCount += 1
            return 1, latency, avgResponseTime
        else:    # if there is no server that can take the task, keep it in a queue and try to place it with the next task placement
            return 0, latency, avgResponseTime

    def _processing_task_placement(self, task: Task, sourceID: int) -> Node:    # returns the data center/server destination chosen by round robin, as well as the latency/average response time due to this placement
        latency = 0
        destination = None
        sourceNode = None
        for source in self.infrastructure.nodes(type_filter=Source):
            if source.id == sourceID:
                sourceNode = source
                break
        dcID = self.placementCount % len(DC_LOCATIONS)
        dcList = self.infrastructure.nodes(type_filter=DataCenter)

        i = dcID
        while destination is None:
            dataC = dcList[i]
            server = self.getServer(task, dataC.serverList)        # Find a physical machine in 'dataC' that can acommodate the request/task
            if server is not None:
                destination = server
                latency = self.findLinkLatency(sourceNode, dataC)
            i = (i + 1) % len(DC_LOCATIONS)
            if i == dcID:
                break

        if destination is not None:
            self.responseTimes.append(latency)
            self.avgResponseTime = sum(self.responseTimes)/len(self.responseTimes)  # update overall total cost of response time 'Cr'. this means average response time
        return destination, latency, self.avgResponseTime

    def findLinkLatency(self, src, dst):    # returns the latency of the connection between a source node and a destination node
        latency = math.inf
        for link in self.infrastructure.links(type_filter=Link):
            if link.src == src and link.dst == dst:
                latency = link.latency
                break
        return latency

    def getServer(self, request: Task, hostList: list[Server]):    # returns the first server in a list of servers able to process a given task
        allocatedHost = None
        for host in hostList:
            if request.mips + host.used_mips <= host.mips:  # if host has enough resources for request choose it
                allocatedHost = host

        return allocatedHost    # returns this value as 'least'