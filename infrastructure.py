import math
import random
from typing import Tuple, Optional

import simpy

from leaf.application import Application, Task
from settings import *
from leaf.infrastructure import Link, Node
from leaf.power import PowerModelLink, PowerModelNode, PowerMeasurement

class DataCenter(Node):         # represents a data center that holds multiple physical machines that can process requests/tasks
    def __init__(self, location: Tuple[int, int], id: int, env: simpy.Environment, offset: int):
        self.location = location
        self.id = id
        self.serverList = []
        self.env = env
        self.baseStaticPower = 0.0
        self.coolingFactor = 1.1      # cooling factor will be between 1.1 (representing PUE with no cooling power usage at all) and 2 (representing PUE with full cooling power usage)
        random.seed(id)
        totalMips = self.setupDC()
        super().__init__(f"dataCenter #{id}", mips=totalMips, power_model=PowerModelNode(DC_STATIC_POWER, DC_STATIC_POWER))
        self.powerData = PowerData(self.env, self, offset)
        self.upgraded = 0
        self.downgradeLockout = 0

    def setupDC(self):      # sets up the servers of the data center at random (always using the same seed) and mirrors their total MIPS capacity on the data center
        totalMips = 0
        for i in range(NUM_SERVERS):
            randIndex = random.randint(0,len(SERVER_TYPES)-1)
            serverType = SERVER_TYPES[randIndex]    # server type (max/min power usage and processing power) is randomized
            newServer = Server(i, self, serverType[0], serverType[1], serverType[2])
            self.serverList.append(newServer)
            newServer.updateCooling(self.coolingFactor)
            totalMips += newServer.mips
            self.baseStaticPower += serverType[1]
        return totalMips


    def add_task(self, task: Task):      # adds a task from the data center and attempts to update the cooling factor accordingly
        # if self.shutdown:
        #    self.shutdown = False
        super().add_task(task)
        if task.id is None:
            self.updateCooling(1)
        else:
            self.updateCooling()

    def remove_task(self, task: Task):      # removes a task from the data center and attempts to update the cooling factor accordingly
        super().remove_task(task)
        # if IDLE_SHUTDOWN_THRESHOLD and self.used_mips == 0:
        #    self.shutdown = True
        if task.id is None:
            self.updateCooling(1)
        else:
            self.updateCooling()

    def updateCooling(self, provisional: int = 0):      # sets the cooling factor of the data center based on the amount of load it/its servers is/are processing
        temp = self.coolingFactor

        '''load levels currently based on ESEER https://en.wikipedia.org/wiki/European_seasonal_energy_efficiency_ratio load levels (0%->25%->50%->75%->100%)
        Cooling levels are represented by 4 distinct stages:
        - low load/low CRAC air flow rate
        - medium-low load/medium CRAC air flow rate
        - medium-high load/high CRAC air flow rate
        - high load/high CRAC air flow rate and Chiller on
        '''
        if self.used_mips > (3*self.mips)/4:    #76%-100% load
            self.coolingFactor = 1.6
        elif self.used_mips > self.mips/2:      #51%-75% load
            self.coolingFactor = 1.3
        elif self.used_mips > self.mips/4:      #26%-50% load  (assumed to be the average load of the data center)
            self.coolingFactor = 1.2         # (14130 * x) - 14130= 2355 -> x = 1.1666... for average setting
        else:
            self.coolingFactor = 1.1            #0%-25% load
        if not provisional:               # 'potentialDowngrade' represents wether this is a potential upgrade or downgrade (0 if a task is being removed, otherwise 1)
            if temp < self.coolingFactor:
                self.upgraded = 1
                self.downgradeLockout = self.env.now + COOLING_DELAY    # set the lockout until the cooling can be downgraded again for COOLING_DELAY minutes
                for server in self.serverList:
                    server.updateCooling(self.coolingFactor)
            elif temp > self.coolingFactor:
                if self.env.now >= self.downgradeLockout:
                    for server in self.serverList:
                        server.updateCooling(self.coolingFactor)
                    pass
                else:
                    self.coolingFactor = temp
            else:
                self.upgraded = 0
        else:
            self.coolingFactor = temp


    def measurePower(self) -> PowerMeasurement:     # measures the total energy consumption of the data center, which includes the sum of the power consumption of all of its servers
        power = super().measure_power()     # data center has no power consumption in this iteration, so only server power measurements matter
        static = power.static
        dynamic = power.dynamic
        for server in self.serverList:
            static += server.measurePower().static
            dynamic += server.measurePower().dynamic
        return (static + dynamic)   # multiply by coolingFactor to add in current cooling energy costs

    def calculateRenewables(self):  # calculates renewable energy availability at current time
        return self.powerData.curMeasurement

    def calculateBrownUsage(self):  # calculates brown energy usage at current time
        brown = self.measurePower() - self.calculateRenewables()
        return brown if brown > 0 else 0

class Server(Node):         # represents individual physical machines in each data center
    def __init__(self, id: int, dataC: DataCenter, maxPower: int, minPower: int, serverMips: int):
        super().__init__(f"server #{id} of dataCenter #{dataC.id}", mips=serverMips, power_model=PowerModelNode(maxPower, minPower)) # set dynamic, static power usage in Watt
        self.dataC = dataC
        self.id = id
        self.application = Application()
        self.maxPowerBase = maxPower
        self.minPowerBase = minPower

    def add_task(self, task: Task):  # adds a task to a server for processing. Also places "shadow" task on datacenter so that it knows its current utilization
        self.dataC.add_task(task)
        super().add_task(task)


    def remove_task(self, task: Task):   # removes task from the server. Also removes "shadow" task from datacenter
        self.dataC.remove_task(task)
        super().remove_task(task)

    def measurePower(self) -> PowerMeasurement:     # measures the current total power consumption of the server
        return self.measure_power()

    def updateCooling(self, coolingFactor: int):    # updates the power consumption of the server based on the current coolingfactor of the data center
        self.power_model.max_power = self.maxPowerBase*coolingFactor
        self.power_model.static_power = self.minPowerBase*coolingFactor

class Source(Node):     # represents a source location that requests/tasks are generated at
    def __init__(self, location: Tuple[int, int], id: int):
        super().__init__(f"Source # {id}", mips=0, power_model=PowerModelNode(0, 0))
        self.id = id
        self.location = location

class Link(Link):       # represents a network link between 2 nodes over which tasks can be migrated
    def __init__(self, src: Node, dst: Node, latency: int):
        super().__init__(src, dst, bandwidth=LINK_BANDWIDTH, latency=latency, power_model=PowerModelLink(0))    # power model is 0 so that link energy cost per bit is ignored in algorithms (always = 0)

class PowerData():
    def __init__(self,
                 env: simpy.Environment,
                 dc: DataCenter,
                 offset: int,
                 measurement_interval: Optional[float] = 1,
                 end_time: Optional[float] = math.inf):
        self.env = env
        self.dc = dc
        self.offset = offset
        self.measurement_interval = measurement_interval
        self.end_time = end_time
        self.measurements = []
        self.curMeasurement = 0.0
        self.prices = []
        self.curPrice = 0.0
        self.process = env.process(self._run())
        self.carbonFootprint = CARBON_INTENSITIES[dc.id]/1000/60     # selects the correct carbon tax from the list corresponding to data center's country location AND converts it from tco2eq/kWh to tco2eq/W
        self.carbonTax = CARBON_TAXES[dc.id]    # selects the correct carbon tax from the list corresponding to data center's country location

    def _run(self):         # reads the green energy and price data file for the given data center at each time interval of the simulation and sets the current electricity price/green energy supply accordingly
        with open(GREEN_DATA_FILE, encoding='utf-8-sig') as greenEnergyFile, open(f'power data/Price data {self.dc.id} (placeholder cleaned).csv', encoding='utf-8-sig') as priceFile:
            readCSVEnergy = greenEnergyFile.readlines()
            readCSVPrice = priceFile.readlines()
            i = self.offset
            while self.env.now < self.end_time:
                line = readCSVEnergy[i % len(readCSVEnergy)].split(",")
                power = (PV_GENERATOR_CAPACITY/30)*abs(float(line[1]))    # renewable availability is scaled up from data from a 30kW PV generator
                self.curMeasurement = power
                self.measurements.append(power)

                line = readCSVPrice[int(i/60)%len(readCSVPrice)].split(",")     # prices change hourly, so the next line is only read after 60 'measurement_interval's (minutes) have passed
                price = float(line[1])
                self.curPrice = price/1000000/60        # price is converted from eur/MWh to eur/Wh and then eur/W
                self.prices.append(self.curPrice)
                i += 1
                yield self.env.timeout(self.measurement_interval)