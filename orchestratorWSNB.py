import math
from math import sqrt, cos, asin, pi
from typing import Tuple

import logging

from infrastructure import DataCenter, Link, Source, Server
from leaf.application import Task
from leaf.infrastructure import Infrastructure, Node
from leaf.orchestrator import Orchestrator
from settings import RESPONSE_TIME_THRESHOLD

logger = logging.getLogger(__name__)

class orchestratorWSNB(Orchestrator):

    def __init__(self, infrastructure: Infrastructure):
        super().__init__(infrastructure)
        self.responseTimes = []
        self.avgResponseTime = 0

    def place(self, task: Task, source: int, threshold=100):    # allocates the given task to the node found by _processing_task_placement(), then returns whether placement was successful or not and response time values
        node, latency, avgResponseTime  = self._processing_task_placement(task, source)
        if node is not None:
            task.id = 1  # task id is set to 1 only at full placement (to set apart tasks that are only being provisionally allocated to calculate power usage during placement)
            task.allocate(node)
            return 1, latency, avgResponseTime
        else:    # if there is no server that can take the task, keep it in a queue and try to place it with the next task placement
            return 0, latency, avgResponseTime

    def _processing_task_placement(self, task: Task, sourceID: int) -> Node:    # returns the data center/server destination chosen by WSNB, as well as the latency/average response time due to this placement
        dataC = None
        minDist = math.inf
        sourceNode = None
        for source in self.infrastructure.nodes(type_filter=Source):
            if source.id == sourceID:
                sourceNode = source
                break
        for center in self.infrastructure.nodes(type_filter=DataCenter):    # find nearest node and set it equal to dataC
            curDist = self.calculateDistance(sourceNode.location, center.location)
            if (dataC == None) or minDist>curDist:
                dataC = center
                minDist = curDist
        latency = self.findLinkLatency(sourceNode, dataC)

        least, bestServer = self.bfdAlgorithm(task, dataC.serverList)        # Find the physical machine in 'dataC' that has the least increased energy consumption 'least'
        destination = bestServer
        renewable = dataC.calculateRenewables()     # Check the amount of available renewable energy 'renewable' of data center 'dataC'
        brownUsage = dataC.calculateBrownUsage()    # (brownusage = brown energy usage of data center 'dataC')
        if renewable > (brownUsage + least):
            pass
        else:
            sortedDCs = sorted(self.infrastructure.nodes(type_filter=DataCenter), key=lambda obj: obj.calculateRenewables(), reverse=True)      # should sort list of data centers found in 'self.infrastructure.nodes(type_filter=DataCenter)' based on available green energy in descending order
            for center in sortedDCs:    # Find the data center 'newCenter' with least latency link from current 'dataC' while also ensuring that Cr + ∆Cr < Tr where ∆Cr = the latency
                least, bestServer = self.bfdAlgorithm(task, center.serverList)
                curResponseTime = self.findLinkLatency(sourceNode, center)
                brownUsage = center.calculateBrownUsage()
                renewable = center.calculateRenewables()
                if bestServer is not None:
                    if (sum(self.responseTimes) + latency)/(len(self.responseTimes)+1) < RESPONSE_TIME_THRESHOLD and latency > curResponseTime and renewable > (brownUsage + least):
                        #print(f"found a center that can cover the request!")
                        latency = curResponseTime
                        newCenter = center
                        destination = bestServer
        if destination is not None:
            self.responseTimes.append(latency)
            self.avgResponseTime = sum(self.responseTimes)/len(self.responseTimes)  # update overall total cost of response time 'Cr'. this means average response time
        return destination, 0, 0

    def findLinkLatency(self, src, dst):    # checks the latency of the connection between a source node and a destination node
        latency = math.inf
        for link in self.infrastructure.links(type_filter=Link):
            if link.src == src and link.dst == dst:
                latency = link.latency
                break
        return latency

    def calculateDistance(self, location1: Tuple[int, int], location2: Tuple[int, int]):    # returns the distance between 2 longitude/latitude tuples in terms of kilometers using Haversine formula
        p = pi / 180
        a = 0.5 - cos((location2[1] - location1[1]) * p) / 2 + cos(location1[1] * p) * cos(location2[1] * p) * (1 - cos((location2[0] - location1[0]) * p)) / 2
        return 12742 * asin(sqrt(a))

    def bfdAlgorithm(self, task: Task, hostList: list[Server]) -> Tuple[float, Server]:    # returns the server from a list of servers that can process the given task and increases the power usage the least, as well as the actual increase in power usage
        minPower = float("inf")
        allocatedHost = None
        for host in hostList:
            if task.mips + host.used_mips <= host.mips:  # if host has enough resources for request and uses less power than previous minPower, choose it
                power = host.measurePower().dynamic
                task.allocate(host)
                power = host.measurePower().dynamic - power
                task.deallocate()
                if power < minPower:
                    allocatedHost = host
                    minPower = power
        return minPower, allocatedHost    # returns the minimum increased energy consumption as 'least' as well as the server chosen