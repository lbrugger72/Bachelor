from tqdm import tqdm
import simpy
from os import makedirs

from infrastructure import DataCenter
from network import Network
from leaf.power import PowerMeter
from settings import SIMULATION_TIME, POWER_MEASUREMENT_INTERVAL

def main(orchestrator: int):        # starts the environment and initializes the network, then sets up measurement tools. Measurements are read out after the simulation conludes and results are all written to several files
    env = simpy.Environment()
    network = Network(env, orchestrator)

    dcMeasurements = []
    for dc in network.infrastructure.nodes(type_filter=DataCenter):
        serverMeterList = []
        for server in dc.serverList:
            serverMeterList.append(_PowerMeter(env, entities=(server), name=f"datacenter #{dc.id}"))
        dcMeasurements.append((dc, _PowerMeter(env, entities=([dc] + dc.serverList), name=f"datacenter #{dc.id}"), serverMeterList))

    for until in tqdm(range(1, SIMULATION_TIME+1)):   # simulation starts here
        env.run(until=until)

    # ------------------ Write results --------------------

    totalSum = 0.0
    totalBrownSum = 0.0
    maxConsumption = 0.0
    totalCost = 0.0
    taxCost = 0.0
    noTaxCost = 0.0
    dynamicConsumption = 0.0
    totalExtraBrown = 0.0
    totalIdle = 0.0
    complementary = 0.0

    for dcMeasurement in dcMeasurements:

        tempSum = 0.0
        tempBrownSum = 0.0
        tempCost = 0.0
        tempExtraBrown = 0.0

        result_dir = f"results/orchestrator {orchestrator} results/data center {dcMeasurement[0].id}"
        makedirs(result_dir, exist_ok=True)
        servers = ""
        for server in dcMeasurement[1].entities[1:]:
            servers += f",s{server.id} static,s{server.id} dynamic"
        total_csv_content = "time,dc static consumption,dc total dynamic consumption" + servers + "\n"
        grid_csv_content = "time,dc total energy consumption,dc green energy availability,dc brown energy consumption,dc energy price (EUR/MWh),energy cost,tax cost,total cost, running cost total\n"
        serverMeasurementsList = []
        for i in range(len(dcMeasurement[1].measurements)):    # create list of measurements from each server
            temp = []
            for server in dcMeasurement[2]:
                temp.append(server.measurements[i])
            serverMeasurementsList.append(temp)
        for j, (dcMeasurements, serverMeasurements) in enumerate(zip(dcMeasurement[1].measurements, serverMeasurementsList)):
            total_csv_content += f"{j},{dcMeasurements.static},{dcMeasurements.dynamic}"

            # various calculations for final measurements (total cost, enrgy consumption, etc.) done below
            tempSum += dcMeasurements.static + dcMeasurements.dynamic
            brownUsage = max(0,dcMeasurements.static + dcMeasurements.dynamic - dcMeasurement[0].powerData.measurements[j])
            tempBrownSum += brownUsage
            if maxConsumption < dcMeasurements.static+dcMeasurements.dynamic:
                maxConsumption = dcMeasurements.static+dcMeasurements.dynamic
            noTaxCost += dcMeasurement[0].powerData.prices[j]*brownUsage
            taxCost += (brownUsage*dcMeasurement[0].powerData.carbonTax*dcMeasurement[0].powerData.carbonFootprint)
            totalCost += (dcMeasurement[0].powerData.prices[j]*brownUsage) + (brownUsage*dcMeasurement[0].powerData.carbonTax*dcMeasurement[0].powerData.carbonFootprint)
            tempCost += totalCost
            dynamicConsumption += max(0,dcMeasurements.dynamic - dcMeasurement[0].powerData.measurements[j])
            if dcMeasurements.dynamic < dcMeasurement[0].powerData.measurements[j]:
                complementary += (dcMeasurement[0].powerData.measurements[j] - dcMeasurements.dynamic)
            tempExtraBrown += max(0,(dcMeasurements.static + dcMeasurements.dynamic - dcMeasurement[0].baseStaticPower) - dcMeasurement[0].powerData.measurements[j])
            totalIdle += dcMeasurement[0].baseStaticPower

            grid_csv_content += f"{j},{dcMeasurements.static+dcMeasurements.dynamic},{dcMeasurement[0].powerData.measurements[j]},{brownUsage},{dcMeasurement[0].powerData.prices[j]},{dcMeasurement[0].powerData.prices[j]*brownUsage},{brownUsage*dcMeasurement[0].powerData.carbonTax*dcMeasurement[0].powerData.carbonFootprint},{(dcMeasurement[0].powerData.prices[j]*brownUsage) + (brownUsage*dcMeasurement[0].powerData.carbonTax*dcMeasurement[0].powerData.carbonFootprint)},{tempCost}\n"    # power usage is converted from Watt to MWh (divided by 60 and then by 1000000) before calculation
            for curServer in serverMeasurements:
                total_csv_content += f",{curServer.static},{curServer.dynamic}"
            total_csv_content += "\n"
        with open(f"{result_dir}/dc {dcMeasurement[0].id} energy consumption breakdown with orchestrator {orchestrator}.csv", 'w') as csvfile:
            csvfile.write(total_csv_content)
        with open(f"{result_dir}/dc {dcMeasurement[0].id} brown energy consumption and prices with orchestrator {orchestrator}.csv", 'w') as csvfile:
            csvfile.write(grid_csv_content)
        with open(f"{result_dir}/dc {dcMeasurement[0].id} location.txt", 'w') as file:
            file.write(f"This data center is located at {dcMeasurement[0].location}")

            totalSum += tempSum
            totalBrownSum += tempBrownSum
            totalExtraBrown += tempExtraBrown

    sourceCountTotal = 0
    distributionCountTotal = 0
    for entry in network.sourceCount:
        sourceCountTotal += entry
    for entry in network.distributionCount:
        distributionCountTotal += entry
    with open(f"results/orchestrator {orchestrator} results/misc orchestrator {orchestrator}.txt", 'w') as file:
        file.write(f"average response time is: {round(network.orchestrator.avgResponseTime, 2)}ms\n"
                   f"total energy consumption was: {round(totalSum/1000, 10)}kW\n"
                   f"total brown energy consumption was: {round(totalBrownSum/1000, 10)}kW\n"
                   f"total brown energy consumption not counting static consumption was: {round(dynamicConsumption/1000, 10)}kW\n"
                   f"            the rest was: {round(complementary/1000, 10)}kW\n"
                   f"total idle energy consumption: {round(totalIdle/1000, 10)}kW\n"
                   f"total max energy consumption anywhere was: {maxConsumption} W\n"
                   f"total cost in euros was: {totalCost}\n"
                   f"total cost breakdown was: {noTaxCost} without carbon taxes and {taxCost} in taxes\n"
                   f"source distribution was: {network.sourceCount}, totaling {sourceCountTotal}\n"
                   f"destination distribution was: {network.distributionCount}, totaling {distributionCountTotal}\n"
                   f"fallback RR strategy was used {network.fallBackCount} times\n"
                   f"The following {len(network.dropppedRequests)} tasks were dropped entirely:\n {network.dropppedRequests}\n")

class _PowerMeter(PowerMeter):
    def __init__(self, env, entities, **kwargs):
        super().__init__(env, entities, measurement_interval=POWER_MEASUREMENT_INTERVAL, **kwargs)


if __name__ == '__main__':
    main(orchestrator=0)    # set orchestrator and whether infrastructure is being measured
    main(orchestrator=1)
    main(orchestrator=2)
    main(orchestrator=3)