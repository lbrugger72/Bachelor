import csv
'''(From left to right below: N. California, Sao Paulo, Frankfurt, Bahrain, Sydney)'''
DC_AVERAGE_PRICES = [105.4,51.8542033834,37.66660046,4,5]       # contains the average price of electricity in MWhrs for at each data center

scalingFactor = DC_AVERAGE_PRICES[2]
csvList= []
for i in range(len(DC_AVERAGE_PRICES)):
    csvList.append([])
with open('Price data (cleaned).csv', encoding='utf-8-sig') as f:
    readCSV = csv.reader(f, delimiter=',')
    for line in readCSV:
        for i, csv_content in enumerate(csvList):
            csv_content.append(f"{line[0]},{float(line[1])*(DC_AVERAGE_PRICES[i] / scalingFactor)}\n")

for i, csv_content in enumerate(csvList):
    csv_content = "".join(csv_content)
    with open(f"Price data {i} (cleaned and scaled).csv", 'w') as csvfile:
        csvfile.write(csv_content)