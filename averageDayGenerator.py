import csv

with open('Green energy data (split and sorted by day of the week).csv', encoding='utf-8-sig') as f:
    readCSV = csv.reader(f, delimiter=',')
    curSum = 0.0
    numEntries = 0.0
    lastTimestamp = None
    lastDate = None
    csv_content = ""
    for line in readCSV:
        if lastDate is None or line[0][0:3] == lastDate[0:3]:
            if line[1] == lastTimestamp or lastTimestamp is None:
                numEntries += 1.0
                curSum += -1.0*float(line[2])
            else:
                csv_content += f"{lastTimestamp},{curSum/numEntries}\n"
                numEntries = 1.0
                curSum = -1.0*float(line[2])
            if lastDate is None:
                lastDate = line[0]
        else:
            csv_content += f"{lastTimestamp},{curSum/numEntries}\n"
            print(f"Last date was {lastDate[0:3]} vs current date {line[0][0:3]} and last timestamp was {lastTimestamp} vs current time {line[1]}")
            #csv_content += f"Day: {lastDate} is ended\n\n"
            lastDate = line[0]
            numEntries = 1.0
            curSum = -1.0*float(line[2])
        lastTimestamp = line[1]

        #if line[0][0:3] == "Sun" and line[1] == "18:23":
        #    print(f"{-1.0*float(line[2])}/{numEntries}={curSum/numEntries}")
        #    print(line)

    print(f"Last date was {lastDate[0:3]} vs current date {line[0][0:3]} and last timestamp was {lastTimestamp} vs current time {line[1]}")
    csv_content += f"{lastTimestamp},{curSum/numEntries}\n"
    #csv_content += f"Day: {lastDate} is ended\n\n"

    with open(f"averageDays.csv", 'w') as csvfile:
        csvfile.write(csv_content)