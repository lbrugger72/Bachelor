import math
from math import sqrt, cos, asin, pi
from typing import Tuple

import logging

from infrastructure import DataCenter, Link, Source, Server
from leaf.application import Task
from leaf.infrastructure import Infrastructure, Node
from leaf.orchestrator import Orchestrator

logger = logging.getLogger(__name__)

class orchestratorCRADP(Orchestrator):

    def __init__(self, infrastructure: Infrastructure):
        super().__init__(infrastructure)
        self.responseTimes = []
        self.avgResponseTime = 0

    def place(self, task: Task, source: int, threshold=100):    # allocates the given task to the node found by _processing_task_placement(), then returns whether placement was successful or not and response time values
        node, latency, avgResponseTime  = self._processing_task_placement(task, source)
        if node is not None:
            task.id = 1  # task id is set to 1 only at full placement (to set apart tasks that are only being provisionally allocated to calculate power usage during placement)
            task.allocate(node)
            return 1, latency, avgResponseTime
        else:    # if there is no server that can take the task, keep it in a queue and try to place it with the next task placement
            return 0, latency, avgResponseTime

    def _processing_task_placement(self, task: Task, sourceID: int) -> Node:    # returns the data center/server destination chosen by CRA-DP, as well as the latency/average response time due to this placement
        destination = None
        sourceNode = None
        for source in self.infrastructure.nodes(type_filter=Source):
            if source.id == sourceID:
                sourceNode = source
                break

        chosenCenter = None
        aggregateDCList: list[Tuple[DataCenter, int]] = []
        for center in self.infrastructure.nodes(type_filter=DataCenter):
            deltaEnergy, curServer = self.getServer(task, center.serverList)
            availGreen = max(0,center.calculateRenewables()-center.calculateBrownUsage())
            # checking of amount of availGreen and whether this is more or less than deltaEnergy (from the original algorithm) is implicit
            usedOffSiteEnergy = deltaEnergy - min(deltaEnergy,availGreen)   # deltaEnergy - usedGreen and usedGreen is either equal to deltaEnergy or availGreen if availGreen < deltaEnergy
            deltaEnergyCost = usedOffSiteEnergy * center.powerData.curPrice     #deltaEnergyCost = Cost of the energy, COE = Price of the off-site grid energy
            deltaCarbonCost = usedOffSiteEnergy * (center.powerData.carbonFootprint * center.powerData.carbonTax)     #deltaCarbonCost = Cost of the carbon footprint, ROE = Carbon footprint rate of off-site grid energy source and TOF = Carbon tax of offsite grid energy source
            deltaTotalCost = deltaEnergyCost + deltaCarbonCost         #deltaTotalCost = Total cost of the energy and carbon
            aggregateDCList.append((center, deltaTotalCost))
        aggregateDCList.sort(key=lambda tuple: tuple[1])     # Sort aggregateDCList in an ascending order of deltaTotalCost

        for center in aggregateDCList:
            powerUsage, chosenServer = self.getServer(task, center[0].serverList)
            if chosenServer is not None:
                chosenCenter = center[0]
                destination = chosenServer
                break

        if destination is not None:
            latency = self.findLinkLatency(sourceNode, chosenCenter)
            self.responseTimes.append(latency)
            self.avgResponseTime = sum(self.responseTimes)/len(self.responseTimes)  # update overall total cost of response time 'Cr'. this means average response time
        return destination, 0, 0

    def findLinkLatency(self, src, dst):    # checks the latency of the connection between a source node and a destination node
        latency = math.inf
        for link in self.infrastructure.links(type_filter=Link):
            if link.src == src and link.dst == dst:
                latency = link.latency
                break
        return latency

    def calculateDistance(self, location1: Tuple[int, int], location2: Tuple[int, int]):    # returns the distance between 2 longitude/latitude tuples in terms of kilometers using Haversine formula
        p = pi / 180
        a = 0.5 - cos((location2[1] - location1[1]) * p) / 2 + cos(location1[1] * p) * cos(location2[1] * p) * (1 - cos((location2[0] - location1[0]) * p)) / 2
        return 12742 * asin(sqrt(a))

    def getServer(self, task: Task, hostList: list[Server]) -> Tuple[float, Server]:    # returns the server from a list of servers that can process the given task and increases the power usage the least, as well as the actual increase in power usage
        minPower = float("inf")
        allocatedHost = None
        sortHosts: list[Tuple[Server,float]] = []
        for host in hostList:
            deltaPower = math.inf
            if task.mips + host.used_mips <= host.mips:
                power = host.measurePower().dynamic
                task.allocate(host)
                powerAfter = host.measurePower().dynamic
                deltaPower = powerAfter - power
                task.deallocate()
            sortHosts.append((host,deltaPower))         # builds list of hosts with expected change in Power
        sortHosts.sort(key=lambda tuple: tuple[1])      # sort hosts in ascending order of deltaPower
        for host in sortHosts:
            if task.mips + host[0].used_mips <= host[0].mips:  # if host is suitable for task
                allocatedHost = host[0]
                minPower = host[1]
                break

        return minPower, allocatedHost    # returns the minimum increased energy consumption as 'least' as well as the server chosen