import math
from typing import Tuple

import logging

from infrastructure import DataCenter, Link, Source, Server
from leaf.application import Task
from leaf.infrastructure import Infrastructure, Node
from leaf.orchestrator import Orchestrator
from settings import BROWN_ENERGY_THRESHOLD

logger = logging.getLogger(__name__)

class orchestratorMinBrown(Orchestrator):

    def __init__(self, infrastructure: Infrastructure):
        super().__init__(infrastructure)
        self.responseTimes = []
        self.avgResponseTime = 0

    def place(self, task: Task, source: int):    # allocates the given task to the node found by _processing_task_placement(), then returns whether placement was successful or not and response time values
        node, latency, avgResponseTime = self._processing_task_placement(task, source)
        if node is not None:
            task.id = 1  # task id is set to 1 only at full placement (to set apart tasks that are only being provisionally allocated to calculate power usage during placement)
            task.allocate(node)
            return 1, latency, avgResponseTime
        else:    # if there is no server that can take the task, keep it in a queue and try to place it with the next task placement
            return 0, latency, avgResponseTime

    def _processing_task_placement(self, task: Task, sourceID: int) -> Node:    # returns the data center/server destination chosen by MinBrown, as well as the latency/average response time due to this placement
        destination = None
        destinationDC = None
        sourceNode = None
        for source in self.infrastructure.nodes(type_filter=Source):
            if source.id == sourceID:
                sourceNode = source
                break

        minBrown = float("inf")
        # slack is ignored, it is assumed that there is always enough time to migrate a task
        for dc in self.infrastructure.nodes(type_filter=DataCenter):
            curConsumption, curServer = self.getServer(task, dc)
            brownConsumption = max(0.0, (dc.measurePower() + curConsumption) - dc.calculateRenewables())
            curBrownRatio = brownConsumption / dc.measurePower()
            if curServer is not None and minBrown > brownConsumption and curBrownRatio < BROWN_ENERGY_THRESHOLD:
                destinationDC = dc
                destination = curServer
                minBrown = brownConsumption

        if destination is not None:
            latency = self.findLinkLatency(sourceNode, destinationDC)
            self.responseTimes.append(latency)
            self.avgResponseTime = sum(self.responseTimes)/len(self.responseTimes)  # update overall total cost of response time 'Cr'. this means average response time
        return destination, 0, 0

    def findLinkLatency(self, src, dst):    # checks the latency of the connection between a source node and a destination node
        latency = math.inf
        for link in self.infrastructure.links(type_filter=Link):
            if link.src == src and link.dst == dst:
                latency = link.latency
                break
        return latency

    def getServer(self, task: Task, dc: DataCenter) -> Tuple[float, Server]:    # returns the first server from a list of servers that will not lead to a cooling upgrade and can process the given task, as well as how much the power usage will increase because of it
        allocatedHost = None
        returnPower = 0
        for host in dc.serverList:
            if task.mips + host.used_mips <= host.mips:  # if host has enough resources for request and uses less power than previous minPower, choose it
                power = host.measurePower().dynamic
                task.allocate(host)
                coolingUpgrade = dc.upgraded       # will be 1 if the last allocation upgraded cooling and 0 if not
                power = host.measurePower().dynamic - power
                task.deallocate()
                if not coolingUpgrade:
                    allocatedHost = host
                    returnPower = power
                    break

        return returnPower, allocatedHost  # returns the increased energy consumption as 'curBrown' as well as the server chosen