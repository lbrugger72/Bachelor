import numpy as np

# DC Nodes
DC_STATIC_POWER = 0    # static power usage of non-IT, non-cooling power usage in Watts -> since data center size is severely scaled down to stay manageable, it would be negligible/near zero
PV_GENERATOR_CAPACITY = 30    # green energy self-generation PV capacity in kW (value is set based on the maximum energy consumption at any DC during the simulation)
'''(From left to right below: N. California, Sao Paulo, Frankfurt, Bahrain, Sydney) MAYBE SWAP OUT SYDNEY FOR JAPAN b/c of data, ALSO NEED CALI, SAO PAULO, AND BAHRAIN DATA'''
DC_LOCATIONS = [(-122.3,37.7), (-46.7,-23.6), (8.6,50.0), (50.5,26.0), (150.7,-34.0)] # shortened data centers list      # latencies taken from https://www.cloudping.co/ with 50th Percentile, 1 Year setting)

# Servers           According to http://www.buyya.com/papers/VMPlacementGeoCloud-TSUSC.pdf "A contemporary server’s idle power is half of the peak power"
'''first server type is less efficient, only 1000MIPS/W, second server type = 1600MIPS/W, third is more efficient = 2000MIPS/W'''
SERVER_TYPES = [(750,375,750000),(1600,800,2560000),(2400,1200,4800000)]        # tuples of (max power consumption, static power consumption, mips) representing different server types (based on different possible setups of the Dell R840 Rack Server and assuming 1600MIPS/W)
NUM_SERVERS = 5

# Edges         # bandwidth isn't factored into the simulation; it is assumed that every task can be transmitted anywhere at any time without limitations
LINK_BANDWIDTH = np.inf

# Sources
'''(From left to right below: N. California, Sao Paulo, Paris, Cape Town, Mumbai, Hong Kong, Tokyo)'''
SRC_LOCATIONS = [(-122.3,37.7), (-46.7,-23.6), (2.3,48.8), (18.5,-33.9), (72.8,19.0), (114.1694,22.3193), (139.7,35.6)] # these are (longitude,latitude) location tuples for every request source
SRC_LATENCY_TABLE = np.array([[2.34,177.43,151.38,264.5,140.0], [177.59,3.65,205.13,332.15,312.0], [143.61,198.17,123.71,10.47,280.25], [318.19,366.38,203.42,226.53,448.79], [237.73,304.1,40.92,119.12,148.96], [159.7,317.52,128.87,210.58,136.55], [111.81,258.19,166.32,240.97,113.46]]) # latencies recorded from the 15.

# Time
SIMULATION_TIME = 11520     # based on difference between first start timestamp and last end timestampt in the current data file [PREVIOUSLY 40.485417 days + 11 minutes total]. 7 days = 10080 minutes + overtime to finish)
POWER_MEASUREMENT_INTERVAL = 1
UPDATE_WIFI_CONNECTIONS_INTERVAL = 60

# Misc
MAX_TASK_MIPS = 1000000     # represents the most memory-intensive task that can be processed. The mips used for each request read from request data is scaled up to this maximum TODO: REPLACE WITH A REASONABLE NUMBER
UTILIZATION_INTERVAL = (0.0,0.04)    # represents the range of values of the normalized memory used by a request read from request data
UTILIZATION_SCALING = MAX_TASK_MIPS/(UTILIZATION_INTERVAL[1]-UTILIZATION_INTERVAL[0])
RESPONSE_TIME_THRESHOLD = 150.0 # response time threshold in ms
COOLING_DELAY = 10      # delay before cooling level is downgraded in minutes
BROWN_ENERGY_THRESHOLD = 0.7
# brown energy percentage threshold for MinBrown NOTE: Varying threshold values only affects the decision of a small number of tasks
'''(From left to right below: N. California, Sao Paulo, Paris, Cape Town, Mumbai, Hong Kong, Seoul, Tokyo)'''
CARBON_INTENSITIES = [0.00018916595, 0.00012037833, 0.0001977644, 0.00018056749, 0.00025795356 ]          # stores the carbon intensity in each data center's country in terms of tco2eq/kWh. 1 gram = 0.000001 tonnes (taken from http://energyatlas.iea.org/#!/tellmap/1378539487/1)
CARBON_TAXES = [8.31,0.80,16.34,0.0,5.63]       # stores the carbon tax in each data center's country in terms of Euros/tco2
GREEN_DATA_FILE = 'power data/green data average.csv'
TASK_DATA_FILE = 'workload data/fixedGoogle2019DataFull (7 days randomized and shortened to 30000).csv'